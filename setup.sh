#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
cd "${SCRIPT_PATH}"

rm -rf app res

mkdir app
cp -r resource/tlapbot/* app/
cp resource/*.py app/
cp resource/*.in app/
cp requirements.txt app/
cp patches/* app/ -r
mkdir res
cp docs/*.svg res/
cp docs/*.ico res/
cp docs/icon.png res/
