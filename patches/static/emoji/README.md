# Tlapka Emojis

Tlapka Emojis was permitted to use by Tlapka (SleepyLili) and it's creators:
 - [FanaticMoose](https://mastodon.online/@FanaticMoose)
 - [bahi0u](https://mk.absturztau.be/@bahi0u)

If you want to use them too, please contact them.
